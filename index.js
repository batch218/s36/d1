// dependencies
const express = require("express");
const mongoose = require("mongoose");

/*
models folder>> task.js
controllers folder >> taskConrollers.js
routes folder > tskRoutes.js
*/

const taskRoute = require("./routes/taskRoute.js");

const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//db connection
mongoose.connect("mongodb+srv://admin:admin@b218-todo.qvht5c6.mongodb.net/toDo?",
  {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

app.use("/tasks",taskRoute);
app.listen(port,() =>console.log(`Now listen to ${port}`));
