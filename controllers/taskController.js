// This Document containss our app feature in displaying and manipulating our database

const { request } = require("express");
const Task = require("../models/task.js");

module.exports.getAllTasks = () => {
  return Task.find({}).then(result =>{
    return result;
  })
};

module.exports.createTasks = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name
  })
  return newTask.save().then((task, error) =>{
    if(error){
      console.log(error);
      return false;
    }else{
      return task;
    }
  })
}
//"taskID" parameter will serve as storage of ID in our url/link
module.exports.deleteTask = (taskID) => {
  return Task.findByIdAndRemove(taskID).then((removedTask, err)=>{
    if(err){
      console.log(err);
      return err;
    }else{
      return removedTask;
    }
  })
  }
  
  module.exports.updateTask = (taskID, newContent) => {
    return Task.findById(taskID).then((result, error)=>{
      if(error){
        console.log(error);
        return error;
      }
      result.name = newContent.name;
      return result.save().then((updateTask,saveErr) =>{
        if(saveErr){
            console.log(saveErr)
            return false;
        }
        else{
          return updateTask;
        }
      })
      
    })
    }

// 1. Create a controller function for retrieving a specific task.
// 2. Create a route 
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
module.exports.specificTask = () => {
  return Task.findOne({}).then(result =>{
    return result;
  })
};

// 5. Create a controller function for changing the status of a task to "complete".
// 6. Create a route
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
module.exports.updateStatus = (taskID, newStatus) => {
  return Task.findById(taskID).then((result, error)=>{
    if(error){
      console.log(error);
      return false;
    }
    result.status = newStatus.status;
    return result.save().then((updateStatus,saveErr) =>{
      if(saveErr){
        console.log(saveErr)
        return false;
      }
      else{
          return updateStatus;
      }
    })
  })
}


// 9. Create a git repository named S31.
// 10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 11. Add the link in Boodle.
